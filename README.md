# Física 3

*Notas de aula do curso de Física 3 - DFI/UEM*

Vamos usar o livro [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://93.174.95.29/main/5E5DC6B6ABE22DE9998530A0667656BD)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iii.pdf).


## Parte 1 - Lei de Coulomb, Campo Elétrico, Potencial Elétrico e Capacitância

- [Aula 01 - 17/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula01.png)
- [Aula 02 - 19/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula02.png)
- [Aula 03 - 24/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula03.png)
- [Aula 04 - 26/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula04.png)
- [Aula 05 - 31/08/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula05.png)
- [Aula 06 - 02/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula06.png)
- [Aula 07 - 09/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula07.png)
- [Aula 08 - 14/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula08.png)
- [Aula 09 - 16/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula09.png)
- [Aula 10 - 21/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula10.png)
- [Aula 11 - 23/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula11.png)
- [Aula 12 - 28/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula12.png)
- [Aula 13 - 30/09/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte1/aula13.png)

## Parte 2 - Corrente Elétrica, Campo Magnético, Lei de Ampère e Lei de Faraday

- [Aula 14 - 05/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula14.png)
- [Aula 15 - 07/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula15.png)
- [Aula 16 - 14/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula16.png)
- [Aula 17 - 19/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula17.png)
- [Aula 18 - 21/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula18.png)
- [Aula 19 - 26/10/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula19.png)
- [28/10/2020] **Prova 1**
- [Aula 20 - 04/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula20.png)
- [Aula 21 - 09/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula21.png)
- [Aula 22 - 11/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula22.png)
- [Aula 23 - 16/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula23.png)
- [Aula 24 - 18/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula24.png)
- [Aula 25 - 23/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula25.png)
- [Aula 26 - 25/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula26.png)
- [Aula 27 - 30/11/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula27.png)
- [Aula 28 - 02/12/2020](https://gitlab.com/hvribeiro/fisica-3/-/blob/master/parte2/aula28.png)
- [07/12/2020] **Dúvidas**
- [09/12/2020] **Dúvidas**
- [14/12/2020] **Dúvidas**
- [16/12/2020] **Prova 2**